#!/bin/bash
#----------------------------------------------------
# Sample Slurm job script
#   for TACC Stampede2 SKX nodes
#   modified from a script supplied by TACC
#
#   *** MPI Job on SKX Normal Queue ***
#
# Notes:
#
#   -- Launch this script by executing
#      "sbatch submit.sh" on Stampede2 login node.
#
#   -- Use ibrun to launch MPI codes on TACC systems.
#      Do not use mpirun or mpiexec.
#
#   -- Max recommended MPI ranks per SKX node: 48
#
#   -- If you're running out of memory, try running
#      fewer tasks per node to give each task more memory.
#
#   This example is for 1 node, using 48 cores per node, on
#   Stampede2's "SKX" nodes (Intel Skylake processors).
#
#   As you know, an equivalent script must be customized for other supercomputers,
#   following the documentation of that particular supercomputer.
#
#   Even on Stampede2 itself, this script must be customized according to your
#   own account and intended run. As an absolute minimum, the words
#
#     - YOUR_ALLOCATION_CODE
#     - YOUR_EMAL_ADDRESS
#     - PATH_TO_YOUR_FHIAIMS_BINARY
#     - VERSION
#
#   must be replaced by information specific to your own account, directory structure
#   and FHI-aims binary file. This script will not function if you do not make those
#   replacements.
#
#----------------------------------------------------
#SBATCH -J aims           # Job name
#SBATCH -o aims.o%j       # Name of stdout output file
#SBATCH -e aims.e%j       # Name of stderr error file
#SBATCH -p skx-normal     # Queue (partition) name skx-large for > 128 nodes
#SBATCH -N 1            # Total # of nodes
#SBATCH --tasks-per-node=48
#SBATCH -t 08:00:00        # Run time (hh:mm:ss)
#SBATCH -A YOUR_ALLOCATION_CODE       # Allocation name (req'd if you have more than 1)
#SBATCH --mail-user=YOUR_EMAIL_ADDRESS
#SBATCH --mail-type=all    # Send email at begin and end of job
# Other commands must follow all #SBATCH directives...
module load intel/19.1.1
module load impi/19.0.9
module list
pwd
date
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1
export MKL_DYNAMIC=FALSE
ulimit -s unlimited
# Launch MPI code...
AIMS=PATH_TO_YOUR_FHI_AIMS_BINARY/aims.VERSION.scalapack.mpi.x
ibrun $AIMS > aims.out         # Use ibrun instead of mpirun or mpiexec
# ---------------------------------------------------
