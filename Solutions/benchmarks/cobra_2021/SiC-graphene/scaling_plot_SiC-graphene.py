# Simple script to plot the log-log scaling behaviour of FHI-aims

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np


def read_aims_output(out_file, output_dic=None):
    """ Read an aims output file and output the summarized timings in a dic.
    Updates the output_dic by appending the read out values."""
    init = False
    if output_dic == None:
        init = True
        output_dic = {}
        output_dic["Number of CPU cores"] = []
        # Init the dic
        output_dic["Init"] = {
            "Total time": [],
            "Integration": [],
            "Grid partitioning": [],
            "Eigenvalue solver": [],
            "Preloading free-atom quantities": []}
        output_dic["1SCF"] = {
            "Total time": [],
            "Integration": [],
            "Density update": [],
            "Hartree potential": [],
            "Eigenvalue solver": []}
        output_dic["Total"] = {
            "Total time": [],
            "Integration": [],
            "Density update": [],
            "Hartree potential": [],
            "Eigenvalue solver": []}
    with open(out_file, "r") as f:
        lines = f.readlines()
    for index, line in enumerate(lines):
        if "parallel tasks." in line:
            output_dic["Number of CPU cores"].append(int(line.split()[1]))
        if "End scf initialization - timings" in line:
            scf_lines = lines[index + 1:index + 9]
            output_dic["Init"]["Total time"].append(float(scf_lines[0].split()[-2]))
            output_dic["Init"]["Integration"].append(float(scf_lines[2].split()[-2]))
            output_dic["Init"]["Grid partitioning"].append(float(scf_lines[4].split()[-2]))
            output_dic["Init"]["Eigenvalue solver"].append(float(scf_lines[3].split()[-2]))
            output_dic["Init"]["Preloading free-atom quantities"].append(float(scf_lines[5].split()[-2]))
        if "End self-consistency iteration #     2" in line:
            scf_lines = lines[index + 1:index + 8]
            output_dic["1SCF"]["Total time"].append(float(scf_lines[0].split()[-2]))
            output_dic["1SCF"]["Integration"].append(float(scf_lines[5].split()[-2]))
            output_dic["1SCF"]["Density update"].append(float(scf_lines[1].split()[-2]))
            output_dic["1SCF"]["Hartree potential"].append(float(scf_lines[4].split()[-2]))
            output_dic["1SCF"]["Eigenvalue solver"].append(float(scf_lines[6].split()[-2]))
        if "Detailed time accounting" in line:
            scf_lines = lines[index + 1:index + 14]
            output_dic["Total"]["Total time"].append(float(scf_lines[0].split()[-2]))
            output_dic["Total"]["Integration"].append(float(scf_lines[6].split()[-2]))
            output_dic["Total"]["Density update"].append(float(scf_lines[8].split()[-2]))
            output_dic["Total"]["Hartree potential"].append(float(scf_lines[11].split()[-2]))
            output_dic["Total"]["Eigenvalue solver"].append(float(scf_lines[7].split()[-2]))

    if init == True:
        return output_dic


settings = ["default", "original"]  # control.in settings
timings = ["Total", "1SCF", "Init"]  # Possible timing outputs

colors = {
    "Total time": "black",
    "Integration": "red",
    "Density update": "blue",
    "Hartree potential": "purple",
    "Eigenvalue solver": "green",
    "Preloading free-atom quantities": "blue",
    "Grid partitioning": "purple"}

markerstyles = {
    "Total time": "^",
    "Integration": "o",
    "Density update": "d",
    "Hartree potential": "d",
    "Eigenvalue solver": "v",
    "Preloading free-atom quantities": "d",
    "Grid partitioning": "d"}

################## Plot the results ############################################
# Prepare a linear scaling line to compare
x = [200, 2200]
y = [1000, 1000 / (x[1] / x[0])]

for setting in settings:
    # Read in data for setting
    file1 = setting + "_settings/reference.cobra.240cores/aims.out"
    file2 = setting + "_settings/reference.cobra.960cores/aims.out"
    file3 = setting + "_settings/reference.cobra.1920cores/aims.out"

    results = read_aims_output(file1, output_dic=None)
    read_aims_output(file2, output_dic=results)
    read_aims_output(file3, output_dic=results)

    for timing in timings:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.set_xlabel("Number of CPU cores", fontsize=15)

        if timing == "Total":
            ax.set_ylabel("Total Time [s]", fontsize=15)
        elif timing == "1SCF":
            ax.set_ylabel("Time for 1 SCF step [s]", fontsize=15)
        elif timing == "Init":
            ax.set_ylabel("Time for SCF initialization [s]", fontsize=15)

        for key in results[timing].keys():
            if key != "Number of CPU cores":
                ax.loglog(results["Number of CPU cores"], results[timing][key], label=key,
                          ls="--", marker=markerstyles[key], color=colors[key],
                          ms=10)
        ax.plot(x, y, label="Linear scaling", color="pink", linewidth=2)

        plt.legend(loc="best")

        ax.set_xlim(200, 2200)
        ax.set_xticks([240, 960, 1920])
        ax.set_xticklabels([240, 960, 1920])
        ax.set_yticks([1, 10, 100, 1000])
        ax.set_yticklabels([1, 10, 100, 1000])

        plt.savefig(
            "SiC-graphene-Cobra_" + setting + "_settings_" + timing + ".png",
            bbox_inches="tight")

        plt.close()
