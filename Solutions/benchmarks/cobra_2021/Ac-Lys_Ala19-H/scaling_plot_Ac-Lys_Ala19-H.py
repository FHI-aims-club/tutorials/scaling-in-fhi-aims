# Simple script to plot the log-log scaling behaviour of FHI-aims

import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter


def read_aims_output(out_file, output_dic=None):
    """ Read an aims output file and output the summarized timings in a dic.
    Updates the output_dic by appending the read out values."""
    init = False
    if output_dic == None:
        init = True
        # Init the dic
        output_dic = {
            "Number of CPU cores": [],
            "Total time": [],
            "Integration": [],
            "Density update": [],
            "Hartree potential": [],
            "Eigenvalue solver": []}
    with open(out_file, "r") as f:
        lines = f.readlines()
    for index, line in enumerate(lines):
        if "parallel tasks." in line:
            output_dic["Number of CPU cores"].append(int(line.split()[1]))
        if "Detailed time accounting" in line:
            scf_lines = lines[index + 1:index + 14]
            output_dic["Total time"].append(float(scf_lines[0].split()[-2]))
            output_dic["Integration"].append(float(scf_lines[6].split()[-2]))
            output_dic["Density update"].append(float(scf_lines[9].split()[-2]))
            output_dic["Hartree potential"].append(float(scf_lines[12].split()[-2]))
            output_dic["Eigenvalue solver"].append(float(scf_lines[7].split()[-2]))
        if "| Number of self-consistency cycles" in line:
            output_dic["scf_iterations"] = int(line.split()[-1])

    if init == True:
        return output_dic


settings = ["original", "default", "specific"]

colors = {
    "Total time": "black",
    "Integration": "red",
    "Density update": "blue",
    "Hartree potential": "purple",
    "Eigenvalue solver": "green"}

markerstyles = {
    "Total time": "^",
    "Integration": "o",
    "Density update": "d",
    "Hartree potential": "d",
    "Eigenvalue solver": "v"}

################## Plot the results ############################################
# Prepare a linear scaling line to compare
x = [30, 300]
y = [1000, 1000 / (x[1] / x[0])]
for setting in settings:
    # Read data in
    file1 = setting + "_settings/" + "reference.cobra.40cores/aims.out"
    file2 = setting + "_settings/" + "reference.cobra.240cores/aims.out"

    results = read_aims_output(file1, output_dic=None)
    read_aims_output(file2, output_dic=results)

    # Actual plotting
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel("Number of CPU cores", fontsize=15)
    ax.set_ylabel(
        "Time for 10 MD steps (" + str(results["scf_iterations"]) + " scf iterations) [s]", fontsize=15)

    for key in results.keys():
        if key != "Number of CPU cores" and key != "scf_iterations":
            ax.loglog(results["Number of CPU cores"], results[key], label=key,
                      ls="--", marker=markerstyles[key], color=colors[key],
                      ms=10)

    ax.plot(x, y, label="Linear scaling", color="pink", linewidth=2)

    plt.legend(loc="best")

    ax.set_xlim(30, 300)
    ax.set_xticks([40, 240])
    ax.set_xticklabels([40, 240])
    ax.set_yticks([1, 10, 100, 1000])
    ax.set_yticklabels([1, 10, 100, 1000])

    plt.savefig("Ac-Ala19-LysH+-Cobra_" + setting + "_settings.png", bbox_inches="tight")

    plt.close()
