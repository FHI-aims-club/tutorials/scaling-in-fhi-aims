This is a short summary of useful keywords in the context of large-scale calculations. Not all of them were mentioned during this tutorial, but may be worth a try for your large-scale workflow.

* `use_local_index`: A must-set keyword for all simulations with more than 100 atoms. **In any large-scale run, this keyword should be used.** This keyword ensures that the memory usage per task (!) does not needlessly scale O(N^2) - a scenario that would otherwise prevent the code from running any larger systems. `use_local_index` can lead to slight slowdown (test to see if this is even the case) but can actually also facilitate speed-ups for large cases.

* `load_balancing`: Can lead to a small speedup when used together with `use_local_index`. Should usually work but using `use_local_index` on its own is also fine.


Things that were not discussed above but may be worth checking:

* `preconditioner kerker off`: For periodic runs, a so called preconditioner is used in the mixing. For systems that are safely non-metallic, this may not be needed and, in large runs, can save noticeable amounts of time. However, note this does need testing especially for very large runs.

* `elsi_restart` keyword: If you are trying to restart a single scf cycle for a very large system, you can use the electronic restart. However, be careful, e.g. you must use ALL k-points when using GGA restart files to start a hybrid run. (In scalar-relativistic DFT-GGA runs of periodic systems, FHI-aims, by default, uses time reversal symmetry to reduce the number of k-points. In order to use all k-points (no symmetry use in the Brillouin zone at all) in preparation of writing restart files for a subsequent hybrid DFT run, set `symmetry_reduced_k_grid .false.`)

* `frozen_core_scf` keyword: In simulations dominated by the eigensolver ("solution of K.-S. equations"), you can achieve substantial speedup at essentially no accuracy loss [^1] by "freezing" core orbitals in the eigenvalue solver. E.g., to freeze all core orbitals deeper than -200 eV, use: 

        frozen_core_scf .true.
        frozen_core_scf_cutoff -200
        
        
[^1]:
  [Yu, Victor Wen-zhe, Jonathan Moussa, and Volker Blum. "Accurate frozen core approximation for all-electron density-functional theory." The Journal of Chemical Physics 154.22 (2021): 224107.](https://doi.org/10.1063/5.0050296)

*  `dry_run` keyword:

What good is a "dry run" (i.e., a test of the input files before running the actual calculations)?

Upon starting the FHI-aims calculation, the code will first check the input
keywords in both `control.in` and `geometry.in` for consistency and (hopefully) 
provide an error message in case of any typographic errors or known inconsistent
keyword combinations that would prevent a correct run of FHI-aims.

In many real world scenarios, FHI-aims will be run through a queueing system, 
i.e., everything will be set up, a job will be queued, the user goes away and waits for
the supercomputer to execute the code. In this case, stopping with a trivial error 
can cost serious amounts of real-world time since the error will need to be corrected 
and the task re-queued.

FHI-aims allows one to check the consistency of input files at the command line without
executing the full run if the keyword

```
dry_run
```

is included in the `control.in` file. In that case, the code will just go through all motions
to set up the calculation but not actually execute the time consuming parts. 

If you're so inclined,
modify your `control.in` file to include the `dry_run` keyword and see what happens if you execute
FHI-aims at the command line. This should tell you if the input files would be accepted for a
later, real-world run, helping you to identify incorrect keywords or typos ahead of time.
