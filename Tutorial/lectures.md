As an introduction to this topic we also recorded two lectures. Please find them below. The corresponding lecture slides can be found here:

* [Ville Havu: Numerical Methods for DFT and Beyond (linear scaling integration and RI)](https://indico.fhi-berlin.mpg.de/event/112/contributions/654/attachments/243/744/Numerical%20methods%20for%20DFT.pdf)
* [Victor Yu: Large-Scale Electronic Structure Methods (ELPA, ELSI)](https://indico.fhi-berlin.mpg.de/event/112/contributions/655/attachments/244/745/211027_fhiaims_large.pdf)

## Ville Havu: Numerical Methods for DFT and Beyond (linear scaling integration and RI)

<iframe width="560" height="315" src="https://www.youtube.com/embed/sGRnLrepkiw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Victor Yu: Large-Scale Electronic Structure Methods (ELPA, ELSI)

<iframe width="560" height="315" src="https://www.youtube.com/embed/r8akDPjru30" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>