# Summary

To set up, run, and produce reproducible results on a supercomputer (actually, any machine) you should be aware of several fundamental aspects. These things will be summarized in the following, briefly.

## Understanding the Supercomputer and its environment

* Get an overview of the supercomputer's architecture (number of nodes, number of CPUs per node, memory per node, type of CPUs, accelerators, walltimes, homogenous/inhomogenous node architecture)
* Which compilers and libraries are available on the computer systems and which ones might be appropriate to compile and link FHI-aims?
* How does the queuing (job submission) system work? 

## Compiling an optimized FHI-aims executable

* Find out, which libraries and compilers are available, including their versions. Unfortunately, some compiler and library combinations can contain bugs that are outside the control of the FHI-aims code itself. Check (at FHI-aims' wiki, located at <https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/home>) if issues are already known for some specific library/compiler versions you might be trying to use.
* Are the environments and available libraries the same during compiling the executable and running the executable? (The executable should be always compiled for the set of instructions available during execution.)
* Use compiler optimization flags (e.g. `-O3` or AVX instruction sets).

## Test your FHI-aims executable

* Run the FHI-aims regression tests for your new executable on your supercomputer. You can find them in the `FHIaims/regression_tests/` folder. 

## Benchmark your FHI-aims executable

* Run benchmark calculations and compare their results to former, known benchmarks while considering the following points:
  1. Reproduce the outcome of a simulation.
  2. Compare the timings.
  3. Check the scaling with the number of CPU cores and/or MPI tasks used. 
  4. (optional) Find the optimal runtime parameters.


<!-- [^1]:
  [Ville Havu, Volker Blum, Paula Havu, and Matthias Scheffler, Efficient O(N) integration for all-electron electronic structure calculation using numerically tabulated basis functions. Journal of Computational Physics 228, 8367-8379 (2009).](http://dx.doi.org/10.1016/j.jcp.2009.08.008)

[^2]:
  [Arvid Ihrig, Jürgen Wieferink, Igor Ying Zhang, Matti Ropo, Xinguo Ren, Patrick Rinke, Matthias Scheffler, and Volker Blum Accurate localized resolution of identity approach for linear-scaling hybrid density functionals and for many-body perturbation theory. New Journal of Physics 17, 093020 (2015).](http://dx.doi.org/10.1088/1367-2630/17/9/093020)

[^3]:
  [Sergey Levchenko, Xinguo Ren, Jürgen Wieferink, Rainer Johanni, Patrick Rinke, Volker Blum, Matthias Scheffler Hybrid functionals for large periodic systems in an all-electron, numeric atom-centered basis framework. Computer Physics Communications 192, 60-69 (2015).](http://dx.doi.org/10.1016/j.cpc.2015.02.021) -->
