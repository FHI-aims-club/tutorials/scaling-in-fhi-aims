# References

A partial list of main FHI-aims references can be obtained from [the basics of running FHI-aims](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/references/). The following list gathers some of the main references regarding scaling in FHI-aims:

* Numerical real-space integration in FHI-aims:

Ville Havu, Volker Blum, Paula Havu, and Matthias Scheffler,
Efficient O(N) integration for all-electron electronic structure calculation using numerically tabulated basis functions.
Journal of Computational Physics 228, 8367-8379 (2009). <http://dx.doi.org/10.1016/j.jcp.2009.08.008>

* Linear-scaling hybrid density functional theory, including in periodic systems:

Arvid Ihrig, Jürgen Wieferink, Igor Ying Zhang, Matti Ropo, Xinguo Ren, Patrick Rinke, Matthias Scheffler, and Volker Blum
Accurate localized resolution of identity approach for linear-scaling hybrid density functionals and for many-body perturbation theory.
New Journal of Physics 17, 093020 (2015). <http://dx.doi.org/10.1088/1367-2630/17/9/093020>

Sergey Levchenko, Xinguo Ren, Jürgen Wieferink, Rainer Johanni, Patrick Rinke, Volker Blum, Matthias Scheffler
Hybrid functionals for large periodic systems in an all-electron, numeric atom-centered basis framework.
Computer Physics Communications 192, 60-69 (2015). <http://dx.doi.org/10.1016/j.cpc.2015.02.021>

* High-performance eigenvalue and density matrix solutions using the ELPA eigenvalue solver and the ELSI electronic structure infrastructure: 

Andreas Marek, Volker Blum, Rainer Johanni, Ville Havu, Bruno Lang, Thomas Auckenthaler, Alexander Heinecke, Hans-Joachim Bungartz, and Hermann Lederer,
The ELPA Library - Scalable Parallel Eigenvalue Solutions for Electronic Structure Theory and Computational Science
The Journal of Physics: Condensed Matter 26, 213201 (2014). <http://stacks.iop.org/0953-8984/26/213201>

Hans-Joachim Bungartz, Christian Carbogno, Martin Galgon, Thomas Huckle, Simone Köcher, Hagen-Henrik Kowalski, Pavel Kus, Bruno Lang, Hermann Lederer, Valeriy Manin, Andreas Marek, Karsten Reuter, Michael Rippl, Matthias Scheffler, Christoph Scheurer,
ELPA: A parallel solver for the generalized eigenvalue problem.
I. Foster et al. (Eds.), Parallel Computing: Technology Trends, 647-668 (IOS Press, 2020).
<https://pure.mpg.de/pubman/item/item_3221243_2/component/file_3221686/APC-36-APC200095.pdf>

Victor Wen-zhe Yu, Fabiano Corsetti, Alberto Garcia, William P. Huhn, Mathias Jacquelin, Weile Jia, Björn Lange, Lin Lin, Jianfeng Lu, Wenhui Mi, Ali Seifitokaldani, Alvaro Vazquez-Mayagoitia, Chao Yang, Haizhao Yang, Volker Blum
ELSI: A Unified Software Interface for Kohn-Sham Electronic Structure Solvers.
Computer Physics Communications 222, 267-285 (2018), DOI: 10.1016/j.cpc.2017.09.007 . <https://doi.org/10.1016/j.cpc.2017.09.007>

Victor Wen-zhe Yu, Carmen Campos, William Dawson, Alberto García, Ville Havu, Ben Hourahine, William P Huhn, Mathias Jacquelin, Weile Jia, Murat Keçeli, Raul Laasner, Yingzhou Li, Lin Lin, Jianfeng Lu, Jonathan Moussa, Jose E Roman, Álvaro Vázquez-Mayagoitia, Chao Yang, Volker Blum
ELSI--An Open Infrastructure for Electronic Structure Solvers.
Computer Physics Communications 256, 107459 (2020). <https://dx.doi.org/10.1016/j.cpc.2020.107459>

Victor Wen-zhe Yu, Jonathan Moussa, Pavel Kůs, Andreas Marek, Peter Messmer, Mina Yoon, Hermann Lederer, Volker Blum
GPU-Acceleration of the ELPA2 Distributed Eigensolver for Dense Symmetric and Hermitian Eigenproblems.
Computer Physics Communications 262, 107808 (2021). <https://doi.org/10.1016/j.cpc.2020.107808>

* GPU acceleration of FHI-aims:

William Huhn, Björn Lange, Victor Wen-zhe Yu, Mina Yoon, Volker Blum
GPGPU Acceleration of All-Electron Electronic Structure Theory Using Localized Numeric Atom-Centered Basis Functions
Computer Physics Communications 254, 107314 (2020). <https://doi.org/10.1016/j.cpc.2020.107314>